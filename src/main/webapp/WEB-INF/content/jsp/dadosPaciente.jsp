<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PlusClinica</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap-theme.min.css" />

<script src="./resources/js/jquery-1.7.js"></script>
<script src="./resources/bootstrap/js/bootstrap.min.js"></script>
<script src="./resources/bootstrap/js/jquery.maskedinput-1.1.4.pack.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#dataNascimento").mask("99/99/9999");
	});
</script>
</head>
<body style="background-color: #2D6BA1;">
	<jsp:include page="menu.jsp" />
	<div class="panel panel-default">
		<div class="panel-heading">
			Paciente&nbsp;<span style="font-size: 16px;"
				class="glyphicon glyphicon-list-alt"></span>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
				<li class=""><a href="#home" data-toggle="tab"
					aria-expanded="false"><span style="font-size: 16px;"
						class="glyphicon glyphicon-search">&nbsp;Filtro Paciente</a></li>
				<li class=""><a href="#profile" data-toggle="tab"
					aria-expanded="false">Anamineses e Evoluções</a></li>
				<li class="active"><a href="#messages" data-toggle="tab"
					aria-expanded="true">Messages</a></li>
				<li class=""><a href="#settings" data-toggle="tab"
					aria-expanded="false">Settings</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane fade" id="home">
					<form action="cadastroUsuario" method="post" name="form"
						role="form">
						<div class="panel-body">
							<div class="col-md-12 row column">
								<div class="col-md-10 row column">
									<div class="col-md-4 column">
										<div class="form-group">
											<strong>Nome:</strong> <input class="form-control"
												name="usuario.nome" type="text" maxlength="100">
										</div>
									</div>
									<div class="col-md-1 column">
										<div class="form-group">
											<br>
											<button class="btn btn-default" type="button">
												<span style="font-size: 16px;"
													class="glyphicon glyphicon-search"></span>
											</button>
										</div>
									</div>
									<div class="col-md-2 column">
										<div class="form-group">
											<strong>Data Nascimento:</strong> <input class="form-control"
												name="usuario.nome" type="text" maxlength="11"
												id="dataNascimento">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="profile">
					
				</div>
				<div class="tab-pane fade active in" id="messages">
				
				</div>
				<div class="tab-pane fade" id="settings">
					
				</div>
			</div>
		</div>
		<!-- /.panel-body -->
		<br>
	</div>
	<!-- /.panel -->
	<jsp:include page="rodape.jsp" />
</body>
</html>