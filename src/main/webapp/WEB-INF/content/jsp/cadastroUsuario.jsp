<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PlusClinica</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap-theme.min.css" />

<script src="./resources/js/jquery-1.7.js"></script>
<script src="./resources/js/bootstrap.js"></script>

</head>
<body style="background-color:#2D6BA1;">
	<jsp:include page="menu.jsp" />
	<form action="cadastroUsuario" method="post" name="form" role="form">
		<div class="container">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="glyphicon glyphicon-user"></i>&nbsp;Cadastro Usuário
					</h3>
				</div>
				<div id="messages">
					<br>
					<c:if test="${not empty error}">
						<div class="alert alert-danger">									
							<a href="#" class="close" data-dismiss="alert" aria-label="close"><i
								class="glyphicon glyphicon-remove"></i></a><strong>Alerta:&nbsp;</strong>${error}										
						</div>
					</c:if>
					<c:if test="${not empty sucesso}">
						<div class="alert alert-success">									
							<a href="#" class="close" data-dismiss="alert" aria-label="close"><i
								class="glyphicon glyphicon-remove"></i></a><strong>${sucesso}</strong>											
						</div>
					</c:if>
				</div>
				<div class="panel-body">
					<div class="col-md-12 row column">
						<div class="col-md-12  column">
							<div class="form-group">
								<strong>Nome:</strong> <input class="form-control"
									name="usuario.nome" type="text" maxlength="100" required="required">
							</div>
						</div>
						<div class="col-md-12 row column">
							<div class="col-md-6 column">
								<div class="form-group">
									<strong>Login:</strong> <input class="form-control"
										name="usuario.login" type="text" maxlength="25" required="required">
								</div>
							</div>
							<div class="col-md-6 column">
								<div class="form-group">
									<strong>Senha:</strong> <input class="form-control"
										name="usuario.senha" type="password" maxlength="25"
										style="size: 100px;" required="required">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 row column">
						<div class="col-md-10 column"></div>
						<div class="col-md-2 column">
							<div class="form-group">
								<button type="submit" class="btn btn-lg btn-primary btn-block">
									<i class="glyphicon glyphicon-floppy-disk"></i> Salvar
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<jsp:include page="rodape.jsp" />
</body>
</html>