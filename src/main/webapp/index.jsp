<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PlusClinica</title>

<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap-theme.min.css" />
<script src="./resources/js/jquery-1.7.js"></script>
<script src="./resources/js/bootstrap.min.js"></script>
</head>
<body background="./resources/img/background1.png">
	<div class="container">

		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<div class="row">

			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Login</h3>
					</div>
					<div class="panel-body">
						<form action="efetuarLogin" method="post" name="form" role="form">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="E-mail" name="email"
										type="email" autofocus>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password"
										name="password" type="password" value="">
								</div>
								<div class="checkbox">
									<label> <input name="remember" type="checkbox"
										value="Remember Me">Lembre-se de mim
									</label>
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<button type="submit" class="btn btn-lg btn-primary btn-block">Entrar</button>

								<br>
								<div class="row">
									<div class="col-md-12">
										<div id="messages">
											<c:if test="${not empty error}">
												<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert"
														aria-label="close"><i
														class="glyphicon glyphicon-remove"></i></a><strong>Alerta:&nbsp;</strong>${error}
												</div>
											</c:if>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>