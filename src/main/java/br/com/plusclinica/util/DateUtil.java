package br.com.plusclinica.util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

/**
 * @author Cesar Medeiros
 * @date 09/01/2016
 */
public class DateUtil {
    private static DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    public static String formatDate(Date date) {
        if (date == null)
            return "";

        return dateFormatter.format(date);
    }

    public static Date localToDate(LocalDate date) {
        if (date == null)
            return null;
        return Date.valueOf(date);
    }

    public static LocalDate dateToLocal(Date date) {
        if (date == null)
            return null;
        return date.toLocalDate();
    }

    public static boolean isBetween(LocalDate date, LocalDate start, LocalDate end) {
        if (date == null || start == null || end == null)
            return false;

        if(date.equals(start) || date.equals(end)) return true;
        
        return date.isAfter(start) && date.isBefore(end);
    }
}