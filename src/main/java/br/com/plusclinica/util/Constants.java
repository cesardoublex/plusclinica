package br.com.plusclinica.util;

import java.awt.Font;

public class Constants {
    public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/plusclinica?user=root&password=Cesar33775644";
    public static final String APP_NAME = "PlusClinica";

    public static final int OS_UNKNOWN = 0;
    public static final int OS_LINUX = 1;
    public static final int OS_WINDOWS = 2;

    public static final Font FONT_MENU = new Font("Arial", Font.BOLD, 16);
}
