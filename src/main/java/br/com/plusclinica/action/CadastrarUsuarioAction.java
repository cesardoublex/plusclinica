package br.com.plusclinica.action;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import br.com.plusclinica.controller.UsuarioController;
import br.com.plusclinica.exception.ExisteUsuarioException;
import br.com.plusclinica.model.Usuario;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Cesar Medeiros
 * @date 07/01/2016
 */ 
public class CadastrarUsuarioAction extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	
	private Usuario usuario;
	
	private UsuarioController usuarioController = UsuarioController.getInstance();
	
	HttpServletRequest request = ServletActionContext.getRequest();
	
	HttpSession session = request.getSession();	
	
	@Action(value = "exibirCadastroUsuario", results = @Result(name = SUCCESS, location = "./jsp/cadastroUsuario.jsp"))
    public String execute() {		
        return SUCCESS;
    } 
	
	@Action(value = "cadastroUsuario", results = @Result(name=SUCCESS, location = "./jsp/cadastroUsuario.jsp"))
	public String cadastrarUsuario() {
		session.removeAttribute("error");
		session.removeAttribute("sucesso");		
		try {
			if(usuarioController.verificarUsuario(usuario) == false){
				usuarioController.salvarUsuario(usuario);
				session.setAttribute("sucesso", Usuario.USUARIO_SUCESS);	
			}else{
				ExisteUsuarioException erro = new ExisteUsuarioException();
				throw erro;
			}			
		} catch (SQLException e) {			
			e.getMessage();
		}catch (ExisteUsuarioException ex) {
			session.setAttribute("error", ex.getMessage());
		}		
		
        return SUCCESS;
    }

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	} 
	
	

}
