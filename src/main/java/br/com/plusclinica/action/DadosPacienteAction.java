package br.com.plusclinica.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

/**
 *@author Cesar Medeiros
 *@date 09/01/2016 
 */
public class DadosPacienteAction extends ActionSupport{
	private static final long serialVersionUID = 1L;

	HttpServletRequest request = ServletActionContext.getRequest();
	
	HttpSession session = request.getSession();	
	
	@Action(value = "exibirDadosPaciente", results = @Result(name = SUCCESS, location = "./jsp/dadosPaciente.jsp"))
    public String execute() {		
        return SUCCESS;
    } 
	
	
}
