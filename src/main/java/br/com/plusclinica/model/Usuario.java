package br.com.plusclinica.model;

import java.util.Objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
/**
 * @author Cesar Medeiros
 * @date 08/01/2016
 */
@DatabaseTable
public class Usuario extends BaseModel{
	
	public final static String USUARIO_SUCESS = "Usuário cadastrado com sucesso.";
	
	@DatabaseField(canBeNull = false)
	private String login;
	
	@DatabaseField(canBeNull = false)
	private String nome;

	@DatabaseField(canBeNull = false)
	private String senha;
	
	@DatabaseField(canBeNull = true,columnName="id_usuario_aud")
	private String usuarioAuditor;
	
	@DatabaseField(canBeNull = true,foreign = true,columnName="tipo_usuario_id")	
	private TipoUsuario tipoUsuario;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
		
	public String getUsuarioAuditor() {
		return usuarioAuditor;
	}

	public void setUsuarioAuditor(String usuarioAuditor) {
		this.usuarioAuditor = usuarioAuditor;
	}

	public boolean verificarLogin(String login, String senha) {
        return Objects.equals(this.login, login) &&
                Objects.equals(this.senha, senha);
    }
	
}
