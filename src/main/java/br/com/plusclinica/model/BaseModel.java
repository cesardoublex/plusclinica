package br.com.plusclinica.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * @author Cesar Medeiros
 * @date 06/01/2016
 */
public class BaseModel {
	@DatabaseField(generatedId = true)
	protected Integer id;

	public BaseModel() {
		super();
	}

	public BaseModel(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}