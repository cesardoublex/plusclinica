package br.com.plusclinica.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Cesar Medeiros
 * @date 08/01/2016
 */
@DatabaseTable
public class TipoUsuario extends BaseModel{

	@DatabaseField(canBeNull = true)
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
