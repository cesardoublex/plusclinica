package br.com.plusclinica.controller;

import java.sql.SQLException;
import java.util.List;

import br.com.plusclinica.model.Usuario;
import br.com.plusclinica.repository.UsuarioDAO;

/**
 * @author Cesar Medeiros
 * @date 09/01/2016
 */
public class UsuarioController implements IUsuarioController {

	private static UsuarioController instance;
	
	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();	
	
	
	public static UsuarioController getInstance() {
        if (instance == null) {
            instance = new UsuarioController();
        }
        return instance;
    }

	public void salvarUsuario(Usuario usuario) {
		try {
			usuarioDAO.insert(usuario);
		} catch (SQLException e) {
			e.getMessage();
		}		
	}

	@Override
	public boolean verificarUsuario(Usuario usuario) throws SQLException {
		
		boolean retorno = false;

		List<Usuario> existeUsuario = usuarioDAO.findUsuarioByLogin(usuario.getLogin());

		if(existeUsuario != null && existeUsuario.size() > 0){
			retorno = true;
		}

		return retorno;
	}
	
	
}
