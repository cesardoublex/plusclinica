package br.com.plusclinica.controller;

import java.sql.SQLException;

import br.com.plusclinica.model.Usuario;

/**
 * @author Cesar Medeiros
 * @date 09/01/2016
 */
public interface IUsuarioController {
	
	/**
	 * Método criado para cadastro de usuarios
	 * 
	 * @author Cesar Medeiros
	 * @date 08/01/2016
	 **/
	public void salvarUsuario(Usuario usuario);
	
	/**
	 * Método criado para rerificar se o usuario está cadastrado
	 * 
	 * @author Cesar Medeiros
	 * @date 08/01/2016
	 **/
	public boolean verificarUsuario(Usuario usuario) throws SQLException;
}
