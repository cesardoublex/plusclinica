package br.com.plusclinica.repository;

import java.sql.SQLException;
import java.util.List;

import br.com.plusclinica.model.Usuario;

/**
 * @author Cesar Medeiros
 * @date 07/01/2016
 */
public interface IUsuarioDAO {
	public List<Usuario> findUsuarioByLogin(String login) throws SQLException;
}
