package br.com.plusclinica.repository;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.com.plusclinica.model.Usuario;

import com.j256.ormlite.stmt.QueryBuilder;

/**
 * @author Cesar Medeiros
 * @date 07/01/2016
 */
public final class UsuarioDAO extends BaseDAO<Usuario> implements IUsuarioDAO {
    private static UsuarioDAO instance;

    public static UsuarioDAO getInstance() {
        if (instance == null) {
            instance = new UsuarioDAO();
        }
        return instance;
    }
    
    private UsuarioDAO() {
        super(Usuario.class);
    }

	@Override
	public List<Usuario> findUsuarioByLogin(String login) throws SQLException {
		
		List<Usuario> usuario = new LinkedList<Usuario>();
		
		QueryBuilder<Usuario, Integer> queryBuilder = dao.queryBuilder();
		queryBuilder.where().eq("login", login);
		usuario = dao.query(queryBuilder.prepare());
		
		return usuario;
	}
}
