package br.com.plusclinica.repository;

import java.sql.SQLException;

import javax.servlet.jsp.jstl.core.Config;

/**
 * @author Cesar Medeiros
 * @date 06/01/2016
 */
public final class ConfigDAO extends BaseDAO<Config> {
    private static ConfigDAO instance;

    public static ConfigDAO getInstance() {
        if (instance == null) {
            instance = new ConfigDAO();
        }

        return instance;
    }

    private ConfigDAO() {
        super(Config.class);
    }

    public boolean isLoginCadastrado() throws SQLException {
        return dao.countOf() > 0L;
    }

    public Config get() throws SQLException {
        return dao.queryBuilder().queryForFirst();
    }
}
