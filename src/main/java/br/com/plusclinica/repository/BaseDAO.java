package br.com.plusclinica.repository;

import java.sql.SQLException;
import java.util.List;

import br.com.plusclinica.util.Constants;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * @author Cesar Medeiros
 * @date 06/01/2016
 */
public abstract class BaseDAO<T> {
    private static ConnectionSource connectionSource;
    protected Dao<T, Integer> dao;

    static {
        try {
            connectionSource = new JdbcConnectionSource(Constants.DATABASE_URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected BaseDAO(Class<T> c) {
        try {
            dao = DaoManager.createDao(getConnection(), c);
            TableUtils.createTableIfNotExists(getConnection(), c);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected ConnectionSource getConnection() {
        return connectionSource;
    }

    public static void closeConnection() {
        if (connectionSource != null) {
            try {
                connectionSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insert(T obj) throws SQLException {
        dao.create(obj);
    }

    public void upsert(T obj) throws SQLException {
        dao.createOrUpdate(obj);
    }

    public void delete(T obj) throws SQLException {
        dao.delete(obj);
    }

    public T findById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public List<T> findAll() throws SQLException {
        return dao.queryForAll();
    }
}
