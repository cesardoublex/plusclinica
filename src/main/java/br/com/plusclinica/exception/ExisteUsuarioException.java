package br.com.plusclinica.exception;

/**
 * @author Cesar Medeiros
 * @date 09/01/2016
 */
public class ExisteUsuarioException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ExisteUsuarioException() {
	       super("Usuário já cadastrado!");
	}
}
